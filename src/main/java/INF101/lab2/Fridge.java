package INF101.lab2;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.ArrayList;

public class Fridge implements IFridge {

    private final int max_items = 20;
    private List<FridgeItem> items;

    public Fridge(){
        items = new ArrayList<FridgeItem>();
    }

    @Override
    public int nItemsInFridge() {
        return items.size();
    }

    @Override
    public int totalSize() {
        return max_items;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        
        if(items.size() >= max_items) {

            return false;
        }
        else {

            return items.add(item);
        }

        
    }

    @Override
    public void takeOut(FridgeItem item) {
        
        //error check
        if (!items.contains(item)){
            throw new NoSuchElementException("This item isn't in the fridge");
        }

        items.remove(item);
    }

    @Override
    public void emptyFridge() {
        items.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {

        List<FridgeItem> expired_items = new ArrayList<FridgeItem>();

        for(FridgeItem item : items){
            if(item.hasExpired()){
                expired_items.add(item);
            }
        }
        for(FridgeItem item : expired_items){
            items.remove(item);
        }

        return expired_items;
    }
    
}
